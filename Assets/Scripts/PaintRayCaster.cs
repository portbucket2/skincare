﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintRayCaster : MonoBehaviour
{

    public Camera cam;
    public TexturePainter texPaint;
    public MeshSnapMan meshSnapMan;
    void Start()
    {
        texPaint.Init();
    }

    public SkinnedMeshRenderer skinMRend;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit rch;
            Ray r = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(r, out rch,100))
            {

                //Debug.Log(rch.textureCoord);
                texPaint.Paint(meshSnapMan.subUV.GetSubUV_Coords_Clamped(rch.textureCoord), skinMRend.material, "filterTex");
            }
        }
    }
}
