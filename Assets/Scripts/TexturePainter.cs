﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TexturePainter
{
    [Header("Settings")]
    public Texture2D sourceTexture;

    [SerializeField] Color brushColor;
    [SerializeField] [Range(10,2000)] float brushSize_pix = 10;
    [SerializeField] [Range(.5f,20)] float brushStrength = 2f;
    [SerializeField] [Range(1,100)] float edgeSharpness = 2f;


    [Header("Active")]
    public Texture2D activeTexture;
    Color[] loadedColors;


    public void Init()
    {
        activeTexture = new Texture2D(sourceTexture.width, sourceTexture.height);
        activeTexture.name = "ActiveTexture";
        loadedColors = sourceTexture.GetPixels();
        activeTexture.SetPixels(loadedColors);
        activeTexture.Apply();
        //this.brushStrength = brushStrength;
        //this.brushSize_pix = brushSize_pix;
        //this.edgeSharpness = edgeSharpness;
        //this.brushColor = brushColor;
    }

    public void Paint(Vector2 centre, Material mat, string texID)
    {
        Vector2Int cntr = new Vector2Int( Mathf.RoundToInt( centre.x * activeTexture.width), Mathf.RoundToInt( centre.y * activeTexture.height));


        float dt = Time.deltaTime;
        int R = activeTexture.height;
        int C = activeTexture.width;
        Vector4 brushColorV4 = brushColor;
        for (int r = 0; r < R; r++)
        {
            for (int c = 0; c < C; c++)
            {
                float d = (cntr.x - c) * (cntr.x - c) + (cntr.y - r) * (cntr.y - r);

                if (d < brushSize_pix)
                {
                    float str =( brushStrength * Mathf.Pow(1-(d/brushSize_pix),1/edgeSharpness))*dt;
                    Vector4 prevCol = loadedColors[r*C + c];

                    if (prevCol.sqrMagnitude > str)
                    {
                        loadedColors[r * C + c] = prevCol + (brushColorV4 - prevCol).normalized * str;
                    }
                    else
                    {
                        loadedColors[r * C + c] = brushColor;
                    }

                }
            }
        }

        activeTexture.SetPixels(loadedColors);

        activeTexture.Apply();

        mat.SetTexture(texID,activeTexture);
       
    }

}
