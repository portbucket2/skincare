﻿using System.Collections.Generic;
using UnityEngine;
public class SubUV
{
    Vector2 centre;
    float scale;
    Vector2 scaleMult;
    float sin;
    float cos;

    public SubUV(Vector2 centre, float scale, Vector2 scaleMult, float revolution)
    {
        SetUpParams(centre, scale, scaleMult, revolution);
    }

    void SetUpParams(Vector2 centre, float scale, Vector2 scaleMult, float revolution)
    {
        this.centre = centre;
        this.scale = scale;
        this.scaleMult = scaleMult;

        this.sin = Mathf.Sin(revolution * 2 * Mathf.PI);
        this.cos = Mathf.Cos(revolution * 2 * Mathf.PI);
    }

    public Vector2 GetSubUV_Coords_Unclamped(Vector2 uv)
    {
        Vector2 rp = uv - centre;

        Vector2 subUV;
        subUV.x = ((rp.x * cos - rp.y * sin) / (scale * scaleMult.x)) + .5f;
        subUV.y = ((rp.x * sin + rp.y * cos) / (scale * scaleMult.y)) + .5f;

        return subUV;
    }
    public Vector2 GetSubUV_Coords_Clamped(Vector2 uv)
    {
        Vector2 rp = uv - centre;

        Vector2 subUV;
        subUV.x = Mathf.Clamp01(((rp.x * cos - rp.y * sin) / (scale * scaleMult.x)) + .5f);
        subUV.y = Mathf.Clamp01(((rp.x * sin + rp.y * cos) / (scale * scaleMult.y)) + .5f);

        return subUV;
    }
}