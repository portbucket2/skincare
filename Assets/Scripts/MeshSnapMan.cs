﻿using System.Collections.Generic;
using UnityEngine;

public class MeshSnapMan : MonoBehaviour
{
    public SkinnedMeshRenderer smRend;
    public Animator anim;
    // Start is called before the first frame update

    public Material smMat;
    public SubUV subUV;


    Mesh mesh;

    public MeshCollider mcol;
    void Start()
    {
        //mesh = new Mesh();
        float xpos = smMat.GetFloat("xpos");
        float ypos = smMat.GetFloat("ypos");
        float scale = smMat.GetFloat("scale");
        float scaleMultX = smMat.GetFloat("scaleMultX");
        float scaleMultY = smMat.GetFloat("scaleMultY");
        float revolution = smMat.GetFloat("revolution");
        subUV = new SubUV(new Vector2(xpos, ypos), scale, new Vector2(scaleMultX, scaleMultY), revolution);

        OnFreeze();
    }

    public void OnFreeze()
    {

        anim.enabled = false;
        mesh = new Mesh();
        smRend.BakeMesh(mesh);
        GameObject go = new GameObject();
        go.transform.position = smRend.transform.position;
        go.transform.rotation = smRend.transform.rotation;
        go.transform.localScale = Vector3.one;

        mcol = go.AddComponent<MeshCollider>();

        mcol.sharedMesh = mesh;
    }
    //float xpos;
    //float ypos;
    //float scale;
    //float scaleMultX;
    //float scaleMultY;
    //float revolution;
    //private void Update()
    //{
    //    xpos = smMat.GetFloat("xpos");
    //    ypos = smMat.GetFloat("ypos");
    //    scale = smMat.GetFloat("scale");
    //    scaleMultX = smMat.GetFloat("scaleMultX");
    //    scaleMultY = smMat.GetFloat("scaleMultY");
    //    revolution = smMat.GetFloat("revolution");
    //    OnBake(new Vector2(xpos, ypos), scale, new Vector2(scaleMultX,scaleMultY), revolution);
    //}

    //List<int> OnBake(Vector2 centre, float scale, Vector2 scaleMult, float revolution)
    //{
    //    List<int> vertsOfInterest = new List<int>();
    //    smRend.BakeMesh(mesh);

    //    Vector2[] uvv0 = mesh.uv;

    //    float sin = Mathf.Sin(revolution * 2 * Mathf.PI);
    //    float cos = Mathf.Cos(revolution * 2 * Mathf.PI);

    //    for (int i = 0; i < uvv0.Length; i++)
    //    {
    //        Vector2 rp = uvv0[i]-centre;

    //        Vector2 finalUV;
    //        finalUV.x = ((rp.x * cos - rp.y * sin)/ (scale * scaleMult.x)) + .5f;
    //        finalUV.y = ((rp.x * sin + rp.y * cos)/ (scale* scaleMult.y)) + .5f;

    //        if (!(finalUV.x < 0 || finalUV.x > 1 || finalUV.y < 0 || finalUV.y > 1))
    //        {
    //            vertsOfInterest.Add(i);
    //        }
    //    }

    //    Debug.LogFormat("OnBake {0}/{1}",vertsOfInterest.Count,uvv0.Length);
    //    return vertsOfInterest;
    //}
}

